import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:space/theme.dart';
import 'package:space/widgets/home_category_item.dart';
import 'package:space/widgets/home_popular_item.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int categoryIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteGreyColor,
      bottomNavigationBar: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: BottomNavigationBar(
            // type: BottomNavigationBarType.fixed,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            backgroundColor: kWhiteColor,
            onTap: (value) {
              if (value == 1) {
                Navigator.pushNamed(context, '/wishlist');
              } else if (value == 2) {
                Navigator.pushNamed(context, '/profile');
              }
            },
            items: [
              BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/Icon/home.png',
                  width: 24,
                  color: kBlueColor,
                ),
                label: 'home',
              ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/Icon/heart.png',
                  width: 24,
                ),
                label: 'wishlist',
              ),
              BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/Icon/person.png',
                  width: 24,
                ),
                label: 'profile',
              ),
            ]),
      ),
      body: Stack(
        children: [
          Image.asset('assets/Icon/meliuk-liuk.png'),
          ListView(
            //list view agar page bisa di scroll
            children: [
              //NOTE HEADER
              Container(
                margin: EdgeInsets.only(top: 24),
                padding: EdgeInsets.symmetric(
                  horizontal: 24,
                ),
                child: Row(
                  children: [
                    Image.asset(
                      'assets/Icon/logo_dark.png',
                      width: 53,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      'Space',
                      style: blackAccentTextStyle.copyWith(
                          fontSize: 20, fontWeight: bold),
                    ),
                    Spacer(),
                    Image.asset(
                      'assets/Icon/shopping-cart.png',
                      width: 30,
                    ),
                  ],
                ),
              ),

              //SEARCH BAR

              GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/search');
                },
                child: Container(
                  margin: EdgeInsets.only(top: 30, left: 24, right: 24),
                  padding: EdgeInsets.all(16),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                    color: kWhiteColor,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Search Furnitue',
                        style: greyTextStyle.copyWith(
                          fontSize: 16,
                          fontWeight: semiBold,
                        ),
                      ),
                      Image.asset(
                        'assets/Icon/search.png',
                        width: 24,
                        color: kGreyColor,
                      ),
                    ],
                  ),
                ),
              ),

              //Category
              Container(
                margin: EdgeInsets.only(top: 30, left: 24, right: 24),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Category',
                      style: blackTextStyle.copyWith(
                          fontSize: 20, fontWeight: semiBold),
                    ),
                    Text(
                      'Show All',
                      style: blackTextStyle.copyWith(
                          fontSize: 14, fontWeight: regular),
                    ),
                  ],
                ),
              ),

              //carousel category
              Container(
                margin: EdgeInsets.only(
                  top: 25,
                ),
                child: CarouselSlider(
                  items: [
                    HomeCategoryItem(
                      title: 'Minimalis Chair',
                      subTitle: 'Chair',
                      imgUrl: 'assets/image_product_cat1.png',
                    ),
                    HomeCategoryItem(
                      title: 'Minimalis Table',
                      subTitle: 'Table',
                      imgUrl: 'assets/image_product_cat2.png',
                    ),
                    HomeCategoryItem(
                      title: 'Minimalis Chair',
                      subTitle: 'Table',
                      imgUrl: 'assets/image_product_cat3.png',
                    ),
                  ],
                  options: CarouselOptions(
                      height: 140,
                      enableInfiniteScroll: false,
                      viewportFraction: 1,
                      onPageChanged: (value, _) {
                        setState(() {
                          categoryIndex = value;
                        });
                      }),
                ),
              ),

              //Indicator Carousel
              Container(
                margin: EdgeInsets.only(
                  top: 13,
                  left: 24,
                  right: 24,
                ),
                child: Row(
                  children: [
                    Container(
                      width: 10,
                      height: 10,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:
                            categoryIndex == 0 ? kBlackColor : kLineDarkColor,
                      ),
                    ),
                    Container(
                      width: 10,
                      height: 10,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:
                            categoryIndex == 1 ? kBlackColor : kLineDarkColor,
                      ),
                    ),
                    Container(
                      width: 10,
                      height: 10,
                      margin: EdgeInsets.only(right: 10),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:
                            categoryIndex == 2 ? kBlackColor : kLineDarkColor,
                      ),
                    ),
                  ],
                ),
              ),

              //POPULAR CARD
              Container(
                  margin: EdgeInsets.only(top: 24),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(40),
                    ),
                    color: kWhiteColor,
                  ),
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: 24,
                          left: 24,
                          right: 24,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'Popular',
                              style: blackTextStyle.copyWith(
                                  fontSize: 24, fontWeight: semiBold),
                            ),
                            Text(
                              'Show All',
                              style: blackTextStyle.copyWith(
                                  fontSize: 14, fontWeight: regular),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      SizedBox(
                        height: 301,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              HomePopularItem(
                                title: 'Poan Chair',
                                imgUrl: 'assets/product_popular1.png',
                                price: 34,
                                isFavorite: true,
                              ),
                              HomePopularItem(
                                title: 'Poan Chair',
                                imgUrl: 'assets/product_popular2.png',
                                price: 34,
                                isFavorite: false,
                              ),
                              HomePopularItem(
                                title: 'Poan Chair',
                                imgUrl: 'assets/product_popular2.png',
                                price: 34,
                                isFavorite: true,
                              ),
                              HomePopularItem(
                                title: 'Poan Chair',
                                imgUrl: 'assets/product_popular1.png',
                                price: 34,
                                isFavorite: true,
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  )),
            ],
          )
        ],
      ),
    );
  }
}
