// import 'dart:html';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:space/theme.dart';

import '../widgets/onboarding_items.dart';

class Onboarding extends StatefulWidget {
  const Onboarding({
    Key? key,
  }) : super(key: key);

  @override
  State<Onboarding> createState() => _OnboardingState();
}

class _OnboardingState extends State<Onboarding> {
  int currentIndex = 0;
  CarouselController controller = CarouselController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: kBlackColor,
      body: Column(children: [
        Expanded(
          child: CarouselSlider(
            items: [
              OnboardingItems(
                imageUrl: 'assets/image_onboarding1.png',
                title: 'Buy Furniture Easily',
                subtitle:
                    'Aliqua id fugiat nostrud irure ex duis ea quis id quis ad et. Sunt qui esse',
              ),
              OnboardingItems(
                imageUrl: 'assets/image_onboarding2.png',
                title: 'Buy Furniture Easily',
                subtitle:
                    'Aliqua id fugiat nostrud irure ex duis ea quis id quis ad et. Sunt qui esse',
              ),
              OnboardingItems(
                imageUrl: 'assets/image_onboarding3.png',
                title: 'Buy Furniture Easily',
                subtitle:
                    'Aliqua id fugiat nostrud irure ex duis ea quis id quis ad et. Sunt qui esse',
              ),
            ],
            options: CarouselOptions(
              height: double.infinity,
              viewportFraction: 1,
              enableInfiniteScroll: false,
              initialPage: currentIndex,
              onPageChanged: (index, _) {
                setState(() {
                  currentIndex = index;
                });
              },
            ),
            carouselController: controller,
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 40),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              TextButton(
                onPressed: () {
                  // Navigator.pushNamed(context, '/sign-in');
                  controller.animateToPage(2); //skip ke page index ke 2
                },
                child: Text(
                  'SKIP',
                  style: blackTextStyle.copyWith(fontSize: 18),
                ),
              ),
              Row(
                children: [
                  Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:
                            currentIndex == 0 ? kBlackColor : kLineDarkColor),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:
                            currentIndex == 1 ? kBlackColor : kLineDarkColor),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Container(
                    width: 10,
                    height: 10,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color:
                            currentIndex == 2 ? kBlackColor : kLineDarkColor),
                  ),
                ],
              ),
              TextButton(
                onPressed: () {
                  if (currentIndex == 2) {
                    Navigator.pushNamed(context, '/sign-in');
                  } else {
                    controller.nextPage();
                  }
                },
                child: Text(
                  'NEXT',
                  style: blackTextStyle.copyWith(fontSize: 18),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 29,
        )
      ]),
    );
  }
}
