import 'package:flutter/material.dart';
import 'package:space/theme.dart';
import 'package:space/widgets/review_item.dart';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  Color indicatorcolor = Color(0xff394A54);
  double indicatormargin = 5;
  int currentindex = 1;
  bool isExpand = false;
  bool isShowReview = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kWhiteGreyColor,
      extendBody: true,
      body: Stack(
        children: [
          Image.asset('assets/Icon/meliuk-liuk.png'),
          Container(
            margin: EdgeInsets.only(top: 95),
            child: Image.asset('assets/image_product_detail$currentindex.png'),
          ),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, '/home');
            },
            child: Container(
              margin: EdgeInsets.only(
                top: 66,
                left: 20,
              ),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: kWhiteColor,
                border: Border.all(
                  color: kLineDarkColor,
                ),
              ),
              child: Icon(
                Icons.chevron_left,
              ),
            ),
          ),
          SizedBox.expand(
            child: DraggableScrollableSheet(
              initialChildSize: 0.4,
              minChildSize: 0.4,
              maxChildSize: 0.95,
              builder: (BuildContext build, ScrollController scrollController) {
                return Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(40),
                    ),
                    color: kWhiteColor,
                  ),
                  child: NotificationListener(
                    onNotification: (Notification notif) {
                      if (notif is ScrollEndNotification) {
                        if (notif.metrics.minScrollExtent == -1.0) {
                          setState(() {
                            isExpand = true;
                          });
                        } else {
                          setState(() {
                            isExpand = false;
                            isShowReview = false;
                          });
                        }
                      }
                      return true;
                    },
                    child: SingleChildScrollView(
                      controller: scrollController,
                      child: Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 24,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 16,
                            ),
                            Center(
                              child: Container(
                                width: 30,
                                height: 4,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(100),
                                    color: kLineDarkColor),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Poang Chair',
                                  style: blackTextStyle.copyWith(
                                    fontSize: 24,
                                    fontWeight: semiBold,
                                  ),
                                ),
                                Text(
                                  '\$219',
                                  style: blackTextStyle.copyWith(
                                    fontSize: 24,
                                    fontWeight: semiBold,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  'IKOE',
                                  style: blackTextStyle.copyWith(
                                    fontSize: 18,
                                    fontWeight: regular,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Container(
                              height: 50,
                              child: Stack(
                                alignment: Alignment.centerLeft,
                                children: [
                                  Row(
                                    children: [
                                      colorIndicator(Color(0xff394A54), 0),
                                      colorIndicator(Color(0xffEBAB23), 1),
                                      colorIndicator(Color(0xff757477), 2),
                                      colorIndicator(Color(0xff29282C), 3),
                                      colorIndicator(Color(0xffECE90A), 4),
                                    ],
                                  ),
                                  AnimatedContainer(
                                    duration: Duration(milliseconds: 200),
                                    height: 40,
                                    width: 40,
                                    margin: EdgeInsets.only(
                                      left: indicatormargin,
                                    ),
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: indicatorcolor,
                                      border: Border.all(
                                        width: 3,
                                        color: kWhiteColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repudiandae fugit, culpa, sapiente similique tenetur consectetur esse error perspiciatis placeat tempora, nostrum ipsam velit nihil dolor molestiae sequi cupiditate aspernatur?",
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: semiBold,
                                height: 2,
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repudiandae fugit, culpa, sapiente similique tenetur consectetur esse error perspiciatis placeat tempora, nostrum ipsam velit nihil dolor molestiae sequi cupiditate aspernatur?",
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: semiBold,
                                height: 2,
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repudiandae fugit, culpa, sapiente similique tenetur consectetur esse error perspiciatis placeat tempora, nostrum ipsam velit nihil dolor molestiae sequi cupiditate aspernatur?",
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: semiBold,
                                height: 2,
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repudiandae fugit, culpa, sapiente similique tenetur consectetur esse error perspiciatis placeat tempora, nostrum ipsam velit nihil dolor molestiae sequi cupiditate aspernatur?",
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: semiBold,
                                height: 2,
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repudiandae fugit, culpa, sapiente similique tenetur consectetur esse error perspiciatis placeat tempora, nostrum ipsam velit nihil dolor molestiae sequi cupiditate aspernatur?",
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: semiBold,
                                height: 2,
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repudiandae fugit, culpa, sapiente similique tenetur consectetur esse error perspiciatis placeat tempora, nostrum ipsam velit nihil dolor molestiae sequi cupiditate aspernatur?",
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: semiBold,
                                height: 2,
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum repudiandae fugit, culpa, sapiente similique tenetur consectetur esse error perspiciatis placeat tempora, nostrum ipsam velit nihil dolor molestiae sequi cupiditate aspernatur?",
                              style: greyTextStyle.copyWith(
                                fontSize: 14,
                                fontWeight: semiBold,
                                height: 2,
                              ),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            isShowReview
                                ? Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Review',
                                        style: blackTextStyle.copyWith(
                                          fontSize: 24,
                                          fontWeight: semiBold,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      ReviewItem(
                                        name: 'Arul',
                                        imageUrl: 'assets/image_reviewer1.png',
                                        review:
                                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                                        items: [
                                          'assets/image_product_review1.png',
                                          'assets/image_product_review2.png',
                                          'assets/image_product_review3.png',
                                        ],
                                      ),
                                      ReviewItem(
                                        name: 'Gusti',
                                        imageUrl: 'assets/image_reviewer3.png',
                                        review:
                                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                                        items: [
                                          'assets/image_product_review1.png',
                                          'assets/image_product_review2.png',
                                          'assets/image_product_review3.png',
                                        ],
                                      ),
                                      ReviewItem(
                                        name: 'Aji',
                                        imageUrl: 'assets/image_reviewer2.png',
                                        review:
                                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                                        items: [
                                          'assets/image_product_review1.png',
                                          'assets/image_product_review2.png',
                                          'assets/image_product_review3.png',
                                        ],
                                      ),
                                      ReviewItem(
                                        name: 'Arul',
                                        imageUrl: 'assets/image_reviewer1.png',
                                        review:
                                            'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
                                        items: [
                                          'assets/image_product_review1.png',
                                          'assets/image_product_review2.png',
                                          'assets/image_product_review3.png',
                                        ],
                                      ),
                                    ],
                                  )
                                : SizedBox()
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
      bottomNavigationBar: isShowReview
          ? null
          : isExpand
              ? Container(
                  width: double.infinity,
                  height: 315,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        kWhiteColor.withOpacity(0.5),
                        kWhiteColor,
                      ],
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {
                          setState(() {
                            isShowReview = true;
                          });
                        },
                        child: Text(
                          "See More",
                          style: blueTextStyle.copyWith(
                            fontSize: 16,
                            fontWeight: semiBold,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: kWhiteColor,
                    boxShadow: [
                      BoxShadow(
                        color: kGreyColor,
                        blurRadius: 5,
                        offset: Offset(0, 2),
                      )
                    ],
                  ),
                  child: Row(
                    children: [
                      Container(
                        width: 56,
                        height: 56,
                        margin: EdgeInsets.only(right: 16),
                        decoration: BoxDecoration(
                          color: kWhiteGreyColor,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: Image.asset(
                            'assets/Icon/shopping-cart.png',
                            width: 24,
                            height: 24,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: double.infinity,
                          height: 56,
                          child: TextButton(
                            style: TextButton.styleFrom(
                              backgroundColor: kBlackColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(14),
                              ),
                            ),
                            onPressed: () {},
                            child: Text(
                              'Buy Now',
                              style: whiteGreyTextStyle.copyWith(
                                fontSize: 18,
                                fontWeight: semiBold,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }

  Widget colorIndicator(Color color, int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          indicatorcolor = color;
          indicatormargin = 5 + (index * 60);
          currentindex = index + 1;
        });
      },
      child: Container(
        height: 50,
        width: 50,
        margin: EdgeInsets.only(
          right: 10,
        ),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color,
        ),
      ),
    );
  }
}
