import 'dart:async';

import 'package:flutter/material.dart';
import 'package:space/theme.dart';
import 'package:space/widgets/profile_menu_item.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  bool isLightMode = true;
  double opacity = 1;

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(microseconds: 400),
      opacity: opacity,
      child: Scaffold(
        backgroundColor: isLightMode ? kWhiteGreyColor : Color(0xff1F1D2B),
        bottomNavigationBar: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: BottomNavigationBar(
              // type: BottomNavigationBarType.fixed,
              showSelectedLabels: false,
              showUnselectedLabels: false,
              backgroundColor: isLightMode ? kWhiteColor : kDarkBackgroudColor,
              onTap: (value) {
                if (value == 0) {
                  Navigator.pushNamed(context, '/home');
                } else if (value == 1) {
                  Navigator.pushNamed(context, '/wishlist');
                }
              },
              items: [
                BottomNavigationBarItem(
                  icon: Image.asset(
                    'assets/Icon/home.png',
                    width: 24,
                    color: isLightMode ? null : kWhiteColor,
                  ),
                  label: 'home',
                ),
                BottomNavigationBarItem(
                  icon: Image.asset(
                    'assets/Icon/heart.png',
                    width: 24,
                    color: isLightMode ? null : kWhiteColor,
                  ),
                  label: 'wishlist',
                ),
                BottomNavigationBarItem(
                  icon: Image.asset(
                    'assets/Icon/person.png',
                    width: 24,
                    color: kBlueColor,
                  ),
                  label: 'profile',
                ),
              ]),
        ),
        body: Stack(
          children: [
            Image.asset(
              'assets/Icon/meliuk-liuk.png',
              color: isLightMode ? null : Color.fromARGB(255, 14, 13, 13),
            ),
            ListView(
              children: [
                SizedBox(
                  height: 130,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 30,
                    right: 24,
                  ),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Image.asset(
                            "assets/image_profile.png",
                            width: 120,
                          ),
                          SizedBox(
                            height: 32,
                          ),
                          Text(
                            "Theresa Webb",
                            style: blackTextStyle.copyWith(
                              fontSize: 24,
                              fontWeight: medium,
                              color: isLightMode ? kBlackColor : kWhiteColor,
                            ),
                          ),
                        ],
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            if (isLightMode == true) {
                              isLightMode = false;
                            } else {
                              isLightMode = true;
                            }
                            opacity = 0;
                          });
                          Timer(Duration(milliseconds: 400), () {
                            setState(() {
                              opacity = 1;
                            });
                          });
                        },
                        child: Container(
                          width: 88,
                          height: 44,
                          padding: EdgeInsets.all(4),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color:
                                isLightMode ? kWhiteColor : kDarkBackgroudColor,
                          ),
                          child: AnimatedAlign(
                              duration: Duration(microseconds: 400),
                              alignment: isLightMode
                                  ? Alignment.centerLeft
                                  : Alignment.centerRight,
                              child: isLightMode
                                  ? Image.asset(
                                      "assets/Icon/icon_switch_dark.png")
                                  : Image.asset(
                                      "assets/Icon/icon_switch_light.png")),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 50),
                  padding: EdgeInsets.symmetric(
                    horizontal: 24,
                    vertical: 32,
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(36),
                    ),
                    color: isLightMode ? kWhiteColor : kDarkBackgroudColor,
                  ),
                  child: Column(
                    children: [
                      ProfileMenuItem(
                        iconUrl: 'assets/Icon/person.png',
                        title: 'My Profile',
                        isLightMode: isLightMode,
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/Icon/map.png',
                        title: 'My Address',
                        isLightMode: isLightMode,
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/Icon/shopping-bag.png',
                        title: 'My Order',
                        isLightMode: isLightMode,
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/Icon/credit-card.png',
                        title: 'Payment Method',
                        isLightMode: isLightMode,
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/Icon/heart.png',
                        title: 'My Wishlist',
                        isLightMode: isLightMode,
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/Icon/question-mark-circle.png',
                        title: 'Frequently Asked Questions',
                        isLightMode: isLightMode,
                      ),
                      ProfileMenuItem(
                        iconUrl: 'assets/Icon/headphones.png',
                        title: 'Customer Care',
                        isLightMode: isLightMode,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
