import 'dart:async';

import 'package:flutter/material.dart';
import 'package:space/theme.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), () {
      Navigator.restorablePushNamedAndRemoveUntil(
          context, '/onboarding', (route) => false);
    });
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: kBlackColor,
        body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 98,
                height: 82,
                margin: EdgeInsets.only(right: 16),
                decoration: BoxDecoration(
                    image: DecorationImage(
                  image: AssetImage('assets/Icon/logo_light.png'),
                )),
              ),
              Text(
                'Space',
                style:
                    whiteGreyTextStyle.copyWith(fontSize: 36, fontWeight: bold),
              )
            ],
          ),
        ));
  }
}
