import 'package:flutter/material.dart';
import 'package:space/theme.dart';

class OnboardingItems extends StatelessWidget {
  final String imageUrl;
  final String title;
  final String subtitle;

  const OnboardingItems(
      {Key? key,
      required this.imageUrl,
      required this.title,
      required this.subtitle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 99),
        Image.asset(
          imageUrl,
          width: double.infinity,
        ),
        SizedBox(
          height: 127,
        ),
        Text(
          title,
          style: blackTextStyle.copyWith(fontSize: 26, fontWeight: bold),
          textAlign: TextAlign.center,
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          subtitle,
          style: greyTextStyle.copyWith(fontSize: 18, fontWeight: regular),
          textAlign: TextAlign.center,
        ),
      ],
    );
  }
}
